## Already done:

- Database (mysql or postgresql) and tools to connect to this database, like **pdadmin**

---

- Java, **version 8**, the JDK

---

- Download the **Play2.5.12** (zip) archive from `https://downloads.typesafe.com/typesafe-activator/1.3.12/typesafe-activator-1.3.12.zip` and extract files. Add path to the play directory.
- check play is available: `activator help`

